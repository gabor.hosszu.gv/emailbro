# Example 03 - Standard email with logging

from email_client import EmailBro

# Set these up yourself to test
USERNAME = 'USERNAMEGOESHERE'
PASSWORD = 'PASSWORDGOESHERER'
YOUR_EMAIL_ADDRESS = 'YOUREMAILADDRESSGOESHERE'
RECIPIENT_EMAIL_ADDRESS = 'TARGETMAILADDRESSGOESHERE'


email = EmailBro('smtp.gmail.com', 465)
email.set_logging(True)
email.login(USERNAME, PASSWORD)
email.send_email(YOUR_EMAIL_ADDRESS, RECIPIENT_EMAIL_ADDRESS, 'Text standard email', 'This is the body of the email', html=False)
email.close()