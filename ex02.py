# Example 02 - quick email (html) and close connection

from email_client import EmailBro

# Set these up yourself to test
USERNAME = 'USERNAMEGOESHERE'
PASSWORD = 'PASSWORDGOESHERER'
YOUR_EMAIL_ADDRESS = 'YOUREMAILADDRESSGOESHERE'
RECIPIENT_EMAIL_ADDRESS = 'TARGETMAILADDRESSGOESHERE'

email_message = {
    'username': USERNAME,
    'password': PASSWORD,
    'sender_address': YOUR_EMAIL_ADDRESS,
    'recipient_address': RECIPIENT_EMAIL_ADDRESS,
    'subject': 'Test HTML email',
    'message': '<h4>This is a test email (HTML)</h4>',
}


email = EmailBro('smtp.gmail.com', 465)
email.quick_email(email_message, html=True, close=True)